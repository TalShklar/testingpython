from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed


values = [2, 3, 4, 5]


def square(n):
   return n * n

def my_print(arg1, arg2):
    return "%s %s" % (arg1, arg2)


def main():

    with ThreadPoolExecutor(max_workers = 3) as executor:
        results = executor.map(my_print, (1,),(3,4))
    for result in results:
        print(result)

if __name__ == '__main__':
   main()