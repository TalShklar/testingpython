from datetime import datetime

from bson import DBRef
from mongoengine import *
from mongoengine import (
    Document as BaseDocument,
    DynamicDocument as BaseDynamnicDocument,
    EmbeddedDocument as BaseEmbeddedDocument
)

class Document(BaseDocument):
    meta = {
        'allow_inheritance': True,
        'abstract': True
    }



class MachineType(Document):
    type = StringField(required=True, unique_with='revision')
    revision = StringField(required=True)
    name = StringField(required=True)
    meta = {
        'allow_inheritance': None,
        'collection': 'machine_types'
    }

class Machine(Document):
    machine_id = StringField(required=True, unique=True)
    url = URLField(required=True, unique=True)
    type = ReferenceField(MachineType, required=True)
    name = StringField(default='')
    description = StringField(default='')
    modified = DateTimeField()
    state = StringField()
    meta = {
        'allow_inheritance': None,
        'collection': 'machines'
    }

class Applicator(Document):
    name = StringField(required=True)
    number = IntField(required=True, unique=True)
    machine_type = ReferenceField(MachineType, required=True)
    description = StringField(default='')
    type = StringField(default='')
    data = DictField(default={})
    meta = {
        'allow_inheritance': True,
        'collection': 'applicators'
    }

class Part(Document):
    name = StringField(required=True)
    number = IntField(required=True, unique=True)
    description = StringField(default='')
    type = StringField(default='')
    # units = StringField(default='mm', choices=('mm',))
    # version = StringField(default="0.1.1")
    # draftVersion = StringField(default="0")
    # cadFileurl = StringField(default="")
    data = DictField(default={})
    meta = {
        'allow_inheritance': True,
        'collection': 'parts'
    }

class Cable(Part):
    type = StringField(default='Cable')

class Plan(EmbeddedDocument):
    applicators = ListField(ReferenceField(Applicator), default=[])
    parts = ListField(ReferenceField(Part), default=[])
    data = DictField(default={})

class Process(Document):
    name = StringField(required=True)
    machine_type = ReferenceField(MachineType, required=True)
    cable = ReferenceField(Cable, required=True)
    description = StringField(default='')
    plan_a = EmbeddedDocumentField(Plan, null=True)
    plan_b = EmbeddedDocumentField(Plan, null=True)
    firstPassYield = IntField(default=1)
    unitsPerHour = IntField(default=1)
    modified = DateTimeField(default=datetime.utcnow)

    meta = {
        'allow_inheritance': None,
        'collection': 'processes'
    }

class MachineToProcess(EmbeddedDocument):
    machine = ReferenceField(Machine, required=True)
    process = ReferenceField(Process, required=True)

class Product(Document):
    name = StringField(required=True)
    number = IntField(required=True, unique=True)
    description = StringField()
    # processes = MapField(ReferenceField(Process), default={})
    processes = EmbeddedDocumentListField(MachineToProcess, default=[])

    meta = {
        'allow_inheritance': None,
        'collection': 'products'
    }

if __name__ == "__main__":
    connect(db='frisimos')
    x = Process.objects().first()

    # # Create
    # new_process = Process(**{
    #     'name':"testing_process2",
    #     'machine_type': '631ee46e047e78f0f8d93a49',
    #     'cable': '63886bfad9d65f35f2a60ba3',
    #     'plan_a': {
    #         'applicators': ['631ef5730a84cb02878ef2f8'],
    #         'parts': ['63cfbb582ea6d0968951be10'],
    #         'data':{
    #             'var1':1
    #         }
    #     }
    # })

    # # Update
    # new_process = Process(**{
    #     'id':'63ede635d05d707f46991497',
    #     'name':"testing_process3",
    #     'machine_type': '631ee47b047e78f0f8d93a4a',
    #     'cable': '63886bfad9d65f35f2a60ba3',
    #     'plan_a': {
    #         'applicators': ['631ef5730a84cb02878ef2f8'],
    #         'parts': ['63cfbb582ea6d0968951be10'],
    #         'data':{
    #             'var2':2
    #         }
    #     }
    # })

    # new_process.save()

    # # Patch
    # process = Process.objects(id='63ede635d05d707f46991497').first()
    #
    # new_process = Process(**{
    #     'name':"testing_process2",
    #     'machine_type': '631ee46e047e78f0f8d93a49',
    #     'cable': '63886bfad9d65f35f2a60ba3',
    #     'plan_a': {
    #         'applicators': ['631ef5730a84cb02878ef2f8'],
    #         'parts': ['63cfbb582ea6d0968951be10'],
    #         'data':{
    #             'var4':4
    #         }
    #     }
    # })
    # j = new_process.to_mongo()
    # process.update(**j)

    # # Create
    # product = Product(**{
    #     'name':"testing_product2",
    #     'number':1,
    #     'processes':[
    #         {
    #             'machine':'631ef6b2815872eccf5f5079',
    #             'process':'6321f171480bf099ca96240e'
    #         }
    #     ]
    # })
    # product.save()

    # Patch
    product = Product.objects(id='63edee16f9de243933805f40').first()
    new_product = Product(**{
        'name':"testing_product3",
        'number':2,
        'processes':[
            {
                'machine':'631ef6b2815872eccf5f5079',
                'process':'6321f171480bf099ca96240e'
            }
        ]
    })
    product.update(**new_product.to_mongo())

    disconnect()