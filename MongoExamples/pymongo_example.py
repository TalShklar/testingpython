from datetime import datetime
from pymongo import MongoClient


if __name__ == "__main__":
    client = MongoClient('mongodb://localhost:27017/')
    db = client['frisimos']
    collection = db['machines']

    data = {}
    data['modified'] = datetime.utcnow()
    data['state'] = "READY"

    mst = collection.find_one({"machine_id": "MST_1"})
    if mst:
        collection.find_one_and_update(
            {"_id" : mst["_id"]},
            {"$set":
                 {"modified": datetime.utcnow(),
                 'state': "READY"},
             }, upsert=True
        )
    client.close()