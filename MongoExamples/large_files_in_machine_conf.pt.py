# POC for saving large binary files as part of machine configuration
# (for example weights for CRP models)
# Tested successfully with 1MB, 10MB, 50MB, 100MB, 200MB, 500MB
import hashlib
from datetime import datetime

import gridfs
import mongoengine
from mongoengine import (
    Document as BaseDocument, StringField, DictField, DateTimeField, connect, disconnect, BinaryField, FileField,
)
from pymongo import MongoClient


class Document(BaseDocument):
    meta = {
        'allow_inheritance': True,
        'abstract': True
    }


class MachineConfiguration(Document):
    name = StringField(default='')
    modified = DateTimeField(default=datetime.utcnow)
    machine_id = StringField(required=True, unique=True)
    data = DictField(default={})
    meta = {
        'allow_inheritance': None,
        'collection': 'machine_configuration2'
    }


if __name__ == "__main__":
    connect(db='frisimos')
    client = MongoClient('mongodb://localhost:27017/')
    db = client['frisimos']
    fs = gridfs.GridFS(db)

    name = 'testing9'

    # Save large file to DB
    file_path = '/tmp/test.bin'
    data = None
    with open(file_path, 'rb') as f:
        data = f.read()
        print("Saving to DB size %d..." % len(data))
        print("file hash: ", hashlib.md5(data).hexdigest())

    # Create Document with reference to file
    obj = MachineConfiguration(name=name, machine_id=name)
    obj.data["file"] = fs.put(data)
    obj.save()

    # Read large file from DB
    print('Loading from DB...')
    obj = MachineConfiguration.objects(name=name).first()
    data = fs.get(obj.data['file']).read()
    print("file hash: ", hashlib.md5(data).hexdigest())

    client.close()
    disconnect()