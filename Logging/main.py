"""
Logging flow:

    call: logger.info()                     Stop
            |                                 |
    Is logger.level > call.level? ------------|
            |                                 |
    Create LogRecord                          |
            |                                 |
    Logger filter ----------------------------|
            |                                 |
    handler = current_logger.handler          |
            |                                 |
--->Is handler.level > LogRecord.level?-------|
|            |                                |
|   Handler filter ---------------------------|
|            |                                |
|   Emit + formatting                         |
|            |                                |
|   Is propagate == True? --------------------|
|            |
----handler = current_logger.parent.handler

:see https://docs.python.org/3/howto/logging.html#logging-flow
"""
import logging, logging.config


def create_log_in_code():
    """
    Create simple logger using python code
    """
    logger = logging.getLogger(__name__ + ".create_log_in_code")
    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(logging.Formatter(
                        fmt="%(asctime)s.%(msecs)03d|%(levelname)-8s|%(name)-25s|%(message)s",
                        datefmt="%y%m%d_%H%M%S"))
    logger.addHandler(ch)
    logger.info("this line should be logged")
    logger.debug("this line should NOT be logged")

def multiple_modules():
    """
    Using logging in multiple modules.
    Configure loggers via dictionary.
    """
    # Configure all loggers:
    # - 'WARNING' messages from all logger will be displayed in console.
    # - 'INFO' messages are displayed in console only for loggers with 'INFO' level and below.
    # - 'DEBUG' messages are saved in file only for loggers with 'DEBUG' level and below.
    #       File size is limited to 5MB, with 1 backup count.
    #       File's format also contains function's and thread's name.
    #
    # Note: If you don't explicitly configure logger's level under "loggers" section, it is likely
    #       to be "WARNING" since it's the default.
    logging.config.dictConfig({
        "version": 1,
        "formatters": {
            "simple": {
                "format": "%(asctime)s.%(msecs)03d|%(levelname)-8s|%(name)-25s|%(message)s",
                "datefmt": "%y%m%d_%H%M%S"
            },
            "detailed": {
                "format": "%(asctime)s.%(msecs)03d|%(levelname)-8s|%(name)-25s|%(funcName)-25s|%(threadName)-10s|%(message)s",
                "datefmt": "%y%m%d_%H%M%S"
            }
        },
        "handlers": {
            "file_debug" : {
                "class": "logging.handlers.RotatingFileHandler",
                "formatter": "detailed",
                "level": "DEBUG",
                "filename": "debug.log",
                "maxBytes": 1024 * 1024 * 5,
                "backupCount" : 1
            },
            "console_info": {
                "class": "logging.StreamHandler",
                "formatter": "simple",
                "level": "INFO"
            },
            "console_warning": {
                "class": "logging.StreamHandler",
                "formatter": "simple",
                "level": "WARNING"
            }

        },
        "loggers": {
            "Modules.module1": {
                "level": "DEBUG",
                "handlers": ["console_info"]
            },
            "Modules.module1.module2": {
                "level": "DEBUG",
                "handlers": ["console_info"]
            },
            "Modules": {
                "level": "DEBUG",
                "handlers": ["console_info"]
            },
        },
        "root": {
            "level": "WARNING",
            "handlers": ["console_warning", "file_debug"]
        }
    })

    logger = logging.getLogger(__name__ + ".multiple_modules")
    logger.info("--- Application started ---")
    import Modules.module1 as m1, Modules.module2 as m2
    m1.f()
    m2.f()
    logger.info("Application ended")


if __name__ == "__main__":
    create_log_in_code()
    multiple_modules()