import logging

logger = logging.getLogger('Modules.module1.module2')

def f():
    logger.info("work started (will propagate to module1 logger)")
    logger.debug("working...")