import logging
import Logging.Modules.module2

logger = logging.getLogger(__name__)

def f():
    logger.info("work started")
    logger.debug("working...")
    Logging.Modules.module2.logger.debug("Hijacked module2 logger :)")