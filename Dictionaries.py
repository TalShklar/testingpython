if __name__ == '__main__':

    # Keys must be immutable!

    # Loop items
    d = {'key1':1, 'key2':2}
    for x,y in d.items():
        print(x,y)

    # Comprehensions
    D = {x: x*2 for x in range (10)}
    print('{x: x*2 for x in range (10)} = %s' % D)