import json

"""
Definitions:
Constrain: A Constrain maps between an action an a logical expression of conditions. The action can be made only if this
           expression is evaluates to true. Between expressions logical operators such as 'And' 'Or' 'Not' can appear.

Condition: A condition is evaluated on an HW element, resulting in a boolean value.
           If the element type is a sensor, it is evaluated on its boolean state.
           If the element type is a motor, it is evaluated on its current position 


        constrain                   and             and
Action --------------> |Condition|  or  |Condition| or   |Condition|
                           |        not      |      not      |
                           |                 |               |
                           |                 |               |
                        Sensor           Sensor             Motor
"""

# TODO: Add full parsing of json constrains.json file
# TODO: Add "range" [x,y] for motor actions. The condition should be evaluated when 'entering' 'moving in' 'exiting'
#       this range ( x <= target <= y ||  target > y && source <= y || target < x && source >= x)

def evaluate_condition(condition: dict, variable_value):
    """Recursively evaluate the logical condition based on the single variable value."""
    if '&&' in condition:
        return all(evaluate_condition(sub_condition, variable_value) for sub_condition in condition['&&'])
    elif '||' in condition:
        return any(evaluate_condition(sub_condition, variable_value) for sub_condition in condition['||'])
    elif '!!' in condition:
        return not evaluate_condition(condition['!!'], variable_value)
    elif '!=' in condition:
        return variable_value != condition['!=']
    elif '==' in condition:
        return variable_value == condition['==']
    elif '>' in condition:
        return variable_value > condition['>']
    elif '<' in condition:
        return variable_value < condition['<']
    elif '>=' in condition:
        return variable_value >= condition['>=']
    elif '<=' in condition:
        return variable_value <= condition['<=']
    else:
        raise ValueError(f"Unknown condition: {condition}")

if __name__ == '__main__':
    # Example of condition for motor positions
    condition = json.loads('''
    {
      "||": [
        {
          "&&": [
            { ">": 1 },
            { "<": 10 }
          ]
        },
        {
          "&&": [
            { ">=": 20 },
            { "<=": 30 },
            { "!=": 25 }
          ]
        },
        { "==": 100 }
      ]
    }
    ''')
    values = [0,5,15,20,25,100,200, True, False]
    for v in values:
        print(f"value {v} result {evaluate_condition(condition, v)}")