import json
from enum import Enum

cable_str="""
{
	"_id": {
		"$oid": "5cd16e3147335316edd60203"
	},
	"version": "0.1.1",
	"type": "Part",
	"subPart": "Cable",
	"name": "CAT5.0",
	"P/N": "0000000",
	"units": "mm",
	"draftVersion": "0",
	"description": "Best cable",
	"cadFileurl": "",
	"cable": {
		"length": {
			"$numberInt": "1000"
		},
		"printedText": "usb",
		"layers": {
			"Layer0": {
				"type": "Complex",
				"component list": {
					"id_000": {
						"Wire": "Stripes"
					},
					"id_001": {
						"Wire": "Stripes"
					},
					"id_002": {
						"Wire": "Stripes"
					},
					"id_003": {
						"Wire": "Stripes"
					},
					"id_004": {
						"Wire": "Solid"
					},
					"id_005": {
						"Wire": "Solid"
					},
					"id_006": {
						"Wire": "Solid"
					},
					"id_007": {
						"Wire": "Solid"
					},
					"id_008": {
						"Group": "Shield",
						"component list": {
							"id_000": {
								"Wire": "Stripes"
							},
							"id_004": {
								"Wire": "Solid"
							}
						}
					},
					"id_009": {
						"Group": "Shield",
						"component list": {
							"id_001": {
								"Wire": "Stripes"
							},
							"id_005": {
								"Wire": "Solid"
							}
						}
					},
					"id_010": {
						"Group": "Shield",
						"component list": {
							"id_002": {
								"Wire": "Stripes"
							},
							"id_006": {
								"Wire": "Solid"
							}
						}
					},
					"id_011": {
						"Group": "Shield",
						"component list": {
							"id_003": {
								"Wire": "Stripes"
							},
							"id_007": {
								"Wire": "Solid"
							}
						}
					}
				},
				"components": {
					"id_000": {
						"label": "Orange-White",
						"x": {
							"$numberDouble": "0.6"
						},
						"y": {
							"$numberDouble": "0.6"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"incolor": "#f1f1f1",
						"outcolors": [
							"#FFA500",
							"#f1f1f1"
						],
						"colorRep": {
							"$numberInt": "5"
						}
					},
					"id_001": {
						"label": "Brown-White",
						"x": {
							"$numberDouble": "0.6"
						},
						"y": {
							"$numberDouble": "-0.6"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"incolor": "#f1f1f1",
						"outcolors": [
							"#A0522D",
							"#f1f1f1"
						],
						"colorRep": {
							"$numberInt": "5"
						}
					},
					"id_002": {
						"label": "Blue-White",
						"x": {
							"$numberDouble": "-0.6"
						},
						"y": {
							"$numberDouble": "0.6"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"incolor": "#f1f1f1",
						"outcolors": [
							"#0055ff",
							"#f1f1f1"
						],
						"colorRep": {
							"$numberInt": "5"
						}
					},
					"id_003": {
						"label": "Green-White",
						"x": {
							"$numberDouble": "-0.6"
						},
						"y": {
							"$numberDouble": "-0.6"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"incolor": "#f1f1f1",
						"outcolors": [
							"#228B22",
							"#f1f1f1"
						],
						"colorRep": {
							"$numberInt": "5"
						}
					},
					"id_004": {
						"label": "Orange",
						"x": {
							"$numberDouble": "1.45"
						},
						"y": {
							"$numberDouble": "0.8"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"outcolor": "#FFA500"
					},
					"id_005": {
						"label": "Brown",
						"x": {
							"$numberDouble": "1.45"
						},
						"y": {
							"$numberDouble": "-0.8"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"outcolor": "#A0522D"
					},
					"id_006": {
						"label": "Blue",
						"x": {
							"$numberDouble": "-1.45"
						},
						"y": {
							"$numberDouble": "0.8"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"outcolor": "#0055ff"
					},
					"id_007": {
						"label": "Green",
						"x": {
							"$numberDouble": "-1.45"
						},
						"y": {
							"$numberDouble": "-0.8"
						},
						"radius": {
							"$numberDouble": "0.2"
						},
						"width": {
							"$numberDouble": "0.15"
						},
						"outcolor": "#228B22"
					},
					"id_008": {
						"IDs": [
							"id_000",
							"id_004"
						],
						"width": {
							"$numberDouble": "0.15"
						},
						"color": "#ffffff",
						"linestyle": "--",
						"hatch": ""
					},
					"id_009": {
						"IDs": [
							"id_001",
							"id_005"
						],
						"width": {
							"$numberDouble": "0.15"
						},
						"color": "#ffffff",
						"linestyle": "--",
						"hatch": ""
					},
					"id_010": {
						"IDs": [
							"id_002",
							"id_006"
						],
						"width": {
							"$numberDouble": "0.15"
						},
						"color": "#ffffff",
						"linestyle": "--",
						"hatch": ""
					},
					"id_011": {
						"IDs": [
							"id_003",
							"id_007"
						],
						"width": {
							"$numberDouble": "0.15"
						},
						"color": "#ffffff",
						"linestyle": "--",
						"hatch": ""
					}
				},
				"radius": {
					"$numberDouble": "2.25"
				}
			},
			"Layer1": {
				"type": "Jacket",
				"component list": {
					"id_100": {
						"Insulation": "Solid"
					}
				},
				"components": {
					"id_100": {
						"radius": {
							"$numberDouble": "2.25"
						},
						"width": {
							"$numberDouble": "0.5"
						}
					}
				},
				"radius": {
					"$numberDouble": "2.75"
				}
			}
		}
	},
	"modified": {
		"$date": {
			"$numberLong": "1567026000000"
		}
	}
}
"""

class ColoredWire:
    def __init__(self, label="", type="", colors=[]):
        # if type != 'Solid' and type != 'Stripes':
        #     raise ServiceException("Unsepported wire type %s" % type)

        self._type = type
        self._label = label
        self._colors = colors

    @property
    def label(self):
        return self._label

    @property
    def label_c_style(self):
        return self.label.replace('-', '_').upper()

    @property
    def type(self):
        return self._type

    @property
    def colors(self):
        return self._colors

    def __repr__(self):
        return "%s %s %s" % (self.label, self.type, self.colors)

def getColoredWires(component_list, components):
    wires = {}

    for id in component_list:
        # Check only Wire components
        if "Wire" in component_list[id]:

            # Get Data
            type = component_list[id]['Wire']
            label = components[id]['label']

            color_key = 'outcolor'
            if type == 'Stripes':
                color_key = 'outcolors'
            colors = components[id][color_key]

            # Append to list
            wires[label] = ColoredWire(label, type, colors)

    return wires



if __name__ == '__main__':
    # cable = json.load(cable_str)
    cable = json.loads(cable_str)
    components_list = cable['cable']['layers']['Layer0']['component list']
    components = cable['cable']['layers']['Layer0']['components']

    wires =  getColoredWires(components_list, components)
    print(*wires.values(), sep="\n")

    for wire in wires.values():
        print(wire.label_c_style)

