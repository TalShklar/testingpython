from random import random

import os

import posix
from pathlib import Path

import shutil

import subprocess

import copy
import json
import struct
import sys
import mongoengine

from datetime import datetime
import time

# from PIL.Image import logger

# from machine.client.command import ClientCommand, CommandTimeoutException

import logging
import argparse
from collections.abc import Iterable

from mongoengine import DateTimeField

def remote_cmd(user:str, destination:str, commands:list, timeout=5, wait:bool = True) -> subprocess.CompletedProcess:
    """
    Run command on a remote host
    :param user: Username for ssh connection
    :param destination: host name or IP address
    :param cmd: command to run
    :param timeout: Optional: number of seconds to wait for command to finish. Default is 5 sec
    :return: CompletedProcess object contains ourput and status code.
    """
    # Combine the commands into a single command
    combined_command = ' && '.join(commands)

    # Create SSH command
    ssh_options = '-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'
    if wait:
        ssh_cmd = f'ssh {user}@{destination} {ssh_options} bash -c "{combined_command}"'
    else:
        ssh_cmd = f'ssh {user}@{destination} {ssh_options} nohup bash -c "{combined_command}" > /dev/null 2>&1 &'

    # Execute the SSH command
    return subprocess.run(ssh_cmd.split(' '), timeout=timeout)


class Testing:
    def __init__(self):
        self._ext_data_def = [(2, 'int'),(2,'float')]
        """list of tuples: (size,type) describing the format of the extended data where
            size:  size in words
            type: int or float """

        self._ext_data = [14558, 15628,14558, 15628]
        """Saves data in extended registers"""

        self._ext_byte_to_read = 0
        """Number of bytes to read"""

        for x, y in self._ext_data_def:
            self._ext_byte_to_read += x

        self._ext_data_convertors = {
            'int': int,
            'float': lambda x : Testing.bin_to_float(bin(x))
        }
        """Dictionary which holds conversion function for each type of extended data"""

    @classmethod
    def deserialize(cls, data : list) -> int:
        """
        Deserialize data that had been received from CMMTST driver.
        :param data: List of words, the first word in contains the LSB and the last is MSB
        :return: A single number
        """
        if len(data) == 0:
            return 0

        result = data[0]
        for i in range(1, len(data)):
            result += (data[i] << (16 ** i))
        return result

    def get_extended_data(self) -> list:
        # Since ext_data is being updated constantly, work on a local copy
        data = copy.deepcopy(self._ext_data)
        result = []

        last_index = 0
        for size, type in self._ext_data_def:
            # Serialize network data
            serialized = Testing.deserialize(data[last_index: last_index+size])
            # Convert
            result.append(self._ext_data_convertors[type](serialized))
            last_index += size
        return result

    @classmethod
    def float_to_bin(cls, num):
        return bin(struct.unpack('!I', struct.pack('!f', num))[0])[2:].zfill(32)

    @classmethod
    def bin_to_float(cls, binary):
        return struct.unpack('!f', struct.pack('!I', int(binary, 2)))[0]

# def client_command(service_name, cmd_name, get_response=True, timeout=8, **kwargs):
#     cmd = ClientCommand(service_name + '/' + cmd_name, **kwargs)
#     cmd.start()
#     if get_response:
#         try:
#             cmd.wait(timeout=timeout)
#         except CommandTimeoutException:
#             return None
#         return cmd.response
#     return True

class B:
    def __init__(self):
        self.xb = 1

class A:
    def __init__(self):
        self.xa = 1
        self.b = B()

class MyException(Exception):
    pass

def get_current_partition_usage_percent():
    # Get disk usage statistics for the partition containing the current directory
    usage = shutil.disk_usage(Path.cwd())

    # Calculate the usage percentage
    usage_percent = (usage.used / usage.total) * 100
    return int(usage_percent)


def create_large_file(path, size_gb=30):
    """
    Create a large file of specified size in GB at the given path by writing data to it.

    :param path: str, the full path including filename where the file should be created
    :param size_gb: int, size of the file in gigabytes (default is 10)
    """
    size_bytes = size_gb * 1024 * 1024 * 1024  # Convert GB to bytes
    chunk_size = 1024 * 1024  # 1MB chunk size
    total_chunks = size_bytes // chunk_size

    try:
        # Ensure the directory exists
        os.makedirs(os.path.dirname(path), exist_ok=True)

        # Create the file by writing chunks of data
        with open(path, 'wb') as f:
            for _ in range(total_chunks):
                f.write(b'\0' * chunk_size)

        print(f"File of size {size_gb}GB created successfully at {path}")

    except IOError as e:
        print(f"Error creating file: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")

class MotorException(Exception):
    pass

def start_motor():
    if random() > 0.1:
        raise MotorException()

def start_conveyor():
    """
    Start conveyor again after stopped because of safety
    """
    # Try several times since controller may still be waking up after safety event.
    for i in range(3):
        try:
            print(i)
            start_motor()
            break
        except MotorException as exception:
            time.sleep(1)
    else:
        raise exception


if __name__ == "__main__":
    start_conveyor()
    # print(get_current_partition_usage_percent())
    #
    # file_path = '/home/tal/projects/executions_folder/big_file1'
    #
    # create_large_file(file_path)
    #
    # print(get_current_partition_usage_percent())



    # # Open file
    # file_path = "/home/tal/line_jobs.json"
    # obj_dict = None
    # with open(file_path, "r") as file:
    #     obj_dict = json.load(file)
    #
    # if obj_dict:
    #     for x in obj_dict:
    #         try:
    #             start_time = str(x['start_time']['$date'])
    #             start_time = start_time.replace('Z','')
    #             start_time = datetime.fromisoformat(start_time).strftime('%d/%m/%y')
    #         except:
    #             start_time = ""
    #
    #         try:
    #             produced = x['statistics']['line_statistics']['produced']
    #         except:
    #             produced = ""
    #
    #         try:
    #             quantity = x['quantity']
    #         except:
    #             quantity = ""
    #
    #         print(f"{start_time}    {produced}  {quantity}")

    # p = Path('/home/tal/projects')
    # print(p.stat())

    # Python program to show working
    # of update() method in Dictionary

    # Dictionary with single item
    # d = {'B': 1, '2':2}
    #
    # d.update(B='For', C='Geeks')
    #
    # print(d)




