import Threading
import threading


class SequenceExecutor:
    def __init__(self):
        self._steps = []
        self._execute_in_steps = False
        self._event = threading.Event()
        self._undo_step = False

    @property
    def execute_in_steps(self):
        return self._execute_in_steps

    @execute_in_steps.setter
    def execute_in_steps(self, value: bool):
        self._execute_in_steps = value

    def add_step(self, step_ref, undo_step_ref):
        self._steps.append((step_ref, undo_step_ref))

    def execute(self):
        step_index = 0
        while step_index < len(self._steps):
            if self._undo_step:
                step_index -= 1

            # Execute
            step = self._steps[step_index]
            if self._undo_step:
                # Undo
                step[1]()
                self._undo_step = False

                step_index -= 1
                if step_index < 0:
                    step_index = 0
            else:
                step[0]()
                step_index += 1

            # Check max steps
            if step_index >= len(self._steps):
                self.execute_in_steps = False

            # Wait if executing in steps
            if self.execute_in_steps:
                print("Waiting")
                self._event.wait()
                self._event.clear()

    def step(self):
        """
        Advance one step in sequence
        """
        self._event.set()

    def undo_step(self):
        """
        Undo last step in sequence
        """
        self._undo_step = True
        self._event.set()


def my_func(*args, **kwargs):
    print(args[0], kwargs.get("arg1"))


if __name__ == "__main__":
    executor = SequenceExecutor()
    executor.add_step(lambda: my_func(1, 2, 3, arg1="bla"), lambda:my_func(-1, -2, -3, arg2="-bla"))
    executor.add_step(lambda: my_func(10, 20, 30, arg1="bla10", arg2="bla20"), lambda: my_func(-10, -20, -30, arg1="-bla10", arg2="-bla20"))
    executor.execute_in_steps = True
    t = Threading.Thread(target=executor.execute)
    t.start()
    executor.undo_step()
    executor.step()
    executor.step()
    # executor.add_step(my_func, 10, 20, 30, arg1="bla10", arg2="bla20")
    # executor.add_step(my_func, 100, 200, 300, arg1="bla100", arg2="bla200")

    # executor.execute_in_steps = True
    #
    # t = Threading.Thread(target=executor.execute)
    # t.start()
    #
    # val = input()
    # val = val.lower()
    # while val != "q":
    #     if val == "s":
    #         executor.step()
    #
    #     if val == "e":
    #         executor.execute_in_steps = False
    #         executor.step()
    #         break
    #
    #     val = input()
    #     val = val.lower()
