import logging
import threading
import time
from threading import Thread


def twoArgs(arg1, arg2):
    print("Recieved args: %s, %s" % (arg1, arg2))

def oneArg(arg1):
    print("Recieved args: %s" % arg1)

class CtrlTelegram(object):
    def __init__(self):
        self._dirty_lock = threading.RLock()
        self._dirty_is_locked = 0

    def __enter__(self):
        got_lock = self._dirty_lock.acquire(blocking=True)
        if not got_lock:
            return None
        self._dirty_is_locked += 1
        return self

    def wait_for_control_lock(self):
        self._dirty_lock.acquire()
        self._dirty_is_locked += 1

    def release_control_lock(self):
        self._dirty_is_locked -= 1
        self._dirty_lock.release()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._dirty_is_locked > 0:
            self._dirty_is_locked -= 1
            self._dirty_lock.release()


def do_work(tel: CtrlTelegram):
    with tel as locked_tel:
        if locked_tel:
            logger.debug("Thread %s acquired lock" % threading.currentThread().getName())
            time.sleep(5)
    logger.debug("Thread %s released lock" % threading.currentThread().getName())


if __name__ == "__main__":
    # logger
    FORMAT = '%(asctime)s %(levelname)-2s %(name)s.%(funcName)s:%(lineno)-5d %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=FORMAT)
    logger = logging.getLogger(__name__)

    # thread = Thread(target=oneArg, args=("sdf",))
    # thread.start()
    # thread.join()

    tel = CtrlTelegram()
    t1 = Thread(target=do_work, args=(tel,), name="t1")
    t1.start()
    time.sleep(2)
    t2 = Thread(target=do_work, args=(tel,), name="t2")
    t2.start()

    t1.join()
    t2.join()