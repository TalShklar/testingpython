import re

if __name__ == "__main__":
    kwargs = {}
    importer_string = 'class:server.resources.importers.phoenix_importer.PhoenixImporter sheet_name:Article'
    for match in re.finditer('(\w+):([\w\.]+)', importer_string):
        if match.groups()[0].lower() == "class":
            class_path = match.groups()[1]
        else:
            kwargs[match.groups()[0]] = match.groups()[1]
    print(class_path)
    print(kwargs)