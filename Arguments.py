def func(x, t = 2, *args, key_word_arg = None, **kwargs):
    """
    :param x: Can be assigned by position or keyword
    :param args: Collects all other positional arguments after x into a tuple.
    :param kwargs:
    :return:
    """
    print(x, t, args, key_word_arg, kwargs)
    # print(args)
    # print(kwargs)

def positional(x,y,z):
    """
    Positional arguments can be assigned by position or name
    """
    print("positional:", x,y,z)

def positional_only(x,y,z,/):
    print("positional_only:", x, y, z)

def keyworded(x=None, y=None, **kwargs):
    print(x,y,kwargs)

if __name__ == "__main__":
    #----------------------
    # Positional arguments:
    # ---------------------
    # Can be assigned by position or name
    positional(1, 2, 3)     # By position: Order matters
    positional(1, z=3, y=2) # By name: Order doesn't matter

    # Once you start assigening  by name, you can not switch back to positional.
    # positional(1, y=2, 3) # SyntaxError: positional argument follows keyword argument

    # Number of arguments must match
    # positional(1, 2)       # Type Error: Missing argument
    # positional(1, 2, 3, 4) # Type Error

    # --------------------------
    # Positional only arguments:
    # --------------------------
    # Can be assigned by position ONLY
    positional_only(1, 2, 3)        # OK
    # positional_only(1, 2, z=3)    # TypeError: got some positional-only arguments passed as keyword arguments

    # --------------------------
    # Keyworded arguments:
    # --------------------------
    keyworded(y=2, x=1, z=3, w=4)





