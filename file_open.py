import tkinter
from tkinter import filedialog

if __name__ == "__main__":
    """
    Display file open dialog
    """
    root = tkinter.Tk()
    root.withdraw()

    file_path = tkinter.filedialog.askopenfilename()