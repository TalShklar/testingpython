import signal
import threading
import time

from flask import current_app, Blueprint

import Threading
from random import random

my_global = None

bp = Blueprint('my_blue_print', __name__)

@bp.route('/my_func', methods=['GET'])
def my_func():
    # Test access to app contect and global
    print('my_func ',current_app.test1, my_global)
    return 'done'

def some_function():
    from my_app import app
    while not stop_event.is_set():
        # Get App context
        with app.app_context():
            # Change global variable
            global my_global
            my_global = int(random() * 10)

            # Test access to app context and global
            print('some_function', current_app.test1, my_global)

        time.sleep(5)

# Start background thread
t = Threading.Thread(target=some_function)
stop_event = threading.Event()
t.start()

# Handle interrupt signal gracefully
def stop_thread(signum, frame):
    stop_event.set()
    original_handler(signum, frame)

original_handler = signal.getsignal(signal.SIGINT)
signal.signal(signal.SIGINT, stop_thread)