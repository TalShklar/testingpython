import os

import Threading

from flask import Flask, request
import my_module

app = Flask(__name__)
app.test1 = 1
app.test2 = 2
port = 9000

# register blueprints
app.register_blueprint(my_module.bp, url_prefix='/my_blue_print')

@app.route('/')
def index():
    return "Welcome! %s on port %s" % (request.args.get('name',"unknown"), port)

if __name__ == '__main__':
    port = os.environ.get('PORT', 9000)
    app.run('127.0.0.1', port)
