"""
__name__  Is set by the interpreter to "__main__" if the current file is the first one to execute by the interpreter.
__name__  Is set to current module's name (file's name) if the current file is imported by another.
"""

# This will run module.py code
import module

print("main.py.__name__ = %s" % __name__)

if __name__ == "__main__":
    print("main.py is being run directly")
else:
    print("main.py is being imported")