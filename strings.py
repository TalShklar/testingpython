# - Strings are immutable
if __name__ == '__main__':
    # Creating
    s = 'string'
    s = str('string')
    s = "string"
    s = """multiline
    string"""
    s = 'with special \t chars\n'   # Backslash for special chars
    s = r'raw \t no special chars'  # Raw: Useful for paths and regex

    # String are sequences, therefore they support:
    # - Access by position
    # - Slicing
    # - Concatenation
    # - Repetition
    s = 'string'
    print(len(s))       # Length
    print(s[0])         # access by position
    print(s[-1])        # == s[len(s)-1]
    print(s[1:3])       # Slicing: Print from index 1 to 3 (not including 3)
    print(s[1:])        # Default of end index is len(s)
    print(s[:3])        # Default of start in index is 0
    print(s[:-1])       # 0 to index 5 (len-1) , not included 5
    print(s[:])         # Whole string
    print (s + ' joining another') # Concatenation
    print(s * 4)                   # Repetition

    # Methods
    print('find: '+ str(s.find('rin')))    # Find
    print(s.replace('rin','RIN'))   # Replace. s is not changed since strings are immutable

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
