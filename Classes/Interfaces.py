from abc import abstractmethod


class Base:
    def on_train(self):
        if not isinstance(self, ITrainable):
            print("not supported")
        else:
            self.train()

    def on_calibrate(self):
        if not isinstance(self, ICalibratable):
            print("not supported")
        else:
            self.calibrate()


class ITrainable:
    @abstractmethod
    def train(self):
        pass


class ICalibratable:
    @abstractmethod
    def calibrate(self):
        pass


class A(Base, ITrainable):
    def train(self):
        print("A is training")


class B(Base, ICalibratable):
    def calibrate(self):
        print("B is calibrating")


if __name__ == "__main__":
    a = A()
    b = B()
    a.on_train()
    a.on_calibrate()
    b.on_train()
    b.on_calibrate()
