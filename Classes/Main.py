class A:
    _z = "testing A"

    def __init__(self):
        self._x = "a_x"
        self._y = "a_y"

    def __str__(self):
        return "_x=%s _y=%s" % (self._x, self._y)

class B(A):

    def __init__(self):
        self._x = "b_x"
        self._y = "b_y"




if __name__ == "__main__":
    a = A()
    b = B()
    print(a)
    print(b)