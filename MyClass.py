class MyClass:

    def __init__(self, name="", number=0, **kwargs):
        self._name = name
        self._number = number

    @property
    def name(self):
        return self._name

    @property
    def number(self):
        return self._number

    @number.setter
    def number(self, value: int):
        if (value < 0 or value > 100):
            return
        self._number = value

    def __str__(self):
        return "name=%s number=%s" % (self._name, self._number)

if __name__ == "__main__":
    x = MyClass("my_name", 50)
    print(x)
